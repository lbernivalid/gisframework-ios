//
//  APIClient.swift
//  GISPod
//
//  Created by VALID Certificadora on 19/03/20.
//

import Foundation

internal class fBBdSIwH95GqQOD8PcbWxh8VLutb4MA2: NSObject {
    private enum XvozoHUyzczmfv8HOtOG8U8adBWwvSgH: Int {
        case v6mvx68rNUIwfltEmShzUbM7wLxZXpdL = -1
        case vji1dJ8gOgbHg1uP2hUWINeuFOyLGHlE = -1206
    }
    
    private enum e3HD3w71nt1gBqbmcijIjY8hfemI4CHJ {
        case HTGIrlvHdeNhr3nAL8HOlMInCBVoS5v1
        case Qb92cxlXl8pVGdjgpMQcnR9aTArfLSB8
        case Hku2p1hlQu5kVtirStF9yqA9JReXxwhA
        case vu2xsd9F3agXSgQt7wispKZolmr54iCj
    }
    
    #if DEV
    static let defaultURL = "https://core-tst.validbio.com.br/"
    #elseif HOMOLOG
    static let defaultURL = "https://core-hml.validbio.com.br/"
    #else
    static let defaultURL = "https://core.validbio.com.br/"
    #endif
    
    static var basicAuth = ""
    
    class func r7eocJib9Ur0jbyqx2eeiNKGQXV2rPdn(username: String, password: String, onCompletion: @escaping onCompletion) {
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.PE3gcNn2FuJw9vqMsq4P6momQpPZWskC()
        vOLRZzT2cIC97dQTJReVEGAJM8filq0A(username: username, password: password) { (data, error) in
            guard let response = data else { onCompletion(false, "Invalid data"); return }
            guard let json = try? JSONDecoder().decode(cUox7VyGxg673mkriMYs1fzdrj7dVKqQ.self, from: response) else { onCompletion(false, "Erro"); return }
            CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.oAuthToken = json.access_token
            y9RHc4KMS6BDJlx5hUOGCMJTQcqES1TZ(token: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.oAuthToken!) { (data, error) in
                guard let info = try? JSONDecoder().decode(GtvctEg08DdhQwPIGZ3SzKyDy85gzuju.self, from: data!) else { onCompletion(false, "Erro"); return }
                CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.contractUUID = info.listOfContractProductInfos.first?.contractUuid
                TTSA8850iF65mCD8TC6mcI2cXg8SMK83(contractUUID: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.contractUUID!, accessToken: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.oAuthToken!) { ( rule, error) in
                    guard let rules = try? JSONDecoder().decode(ROlbQmcmKmNlRvXmlioetdOwtxkUpZoz.self, from: rule!) else { onCompletion(false, "Erro"); return }
                    CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.configs = ZEcwwGJPpbQ63JmW7F13e2m5rjNaABBY(id: (rules.configuration!.id), provider: rules.configuration?.provider ?? .DOT)
                    onCompletion(true, nil)
                }
            }
        }
    }
    
    
    private class func u7cnXd4PS4n6whTd2BzlogYU0k6diM2A () -> URLSession {
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 60.0
        sessionConfig.timeoutIntervalForResource = 300.0
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        let username = "validbio-oauth2-client"
        let password = "validbio-oauth2-client-clivalidbio"
        let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
        basicAuth = loginData.base64EncodedString()
        
        return session
    }
    
    private class func vOLRZzT2cIC97dQTJReVEGAJM8filq0A (username: String, password: String, completionHandler: @escaping onCompletionData) {
        guard let url = URL(string: defaultURL + "oauth/token") else { return }
        let session = u7cnXd4PS4n6whTd2BzlogYU0k6diM2A()
        var request = URLRequest(url: url)
        let boundary = "Boundary-\(UUID().uuidString)"
        
        request.httpBody = XIRFsNufBin8zdBYAPV3nCl8Vi3NA9ff(boundary: boundary, username: username, password: password)
        request.httpMethod = "POST"
        request.addValue("Basic \(basicAuth)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=--------------------------441134357055693654586910", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
        
    }
    
    private class func y9RHc4KMS6BDJlx5hUOGCMJTQcqES1TZ (token: String, completionHandler: @escaping onCompletionData) {
        guard let url = URL(string: defaultURL + "info") else { return }
        let session = u7cnXd4PS4n6whTd2BzlogYU0k6diM2A()
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    private class func TTSA8850iF65mCD8TC6mcI2cXg8SMK83 (contractUUID: String, accessToken: String, completionHandler: @escaping onCompletionData){
        guard let url = URL(string: defaultURL + "rule-manager/rules/\(contractUUID)/GIS_ONBOARDING") else { return }
        let session = u7cnXd4PS4n6whTd2BzlogYU0k6diM2A()
        var request = URLRequest(url: url)
        
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    internal class func TbdQdo2WxXg2mn5u1nvpMpmoWnvoT1L8 (transaction: String, completionHandler: @escaping onCompletionData){
        guard let url = URL(string: defaultURL + "core-op/status/\(transaction)") else { return }
        let session = u7cnXd4PS4n6whTd2BzlogYU0k6diM2A()
        var request = URLRequest(url: url)
        
        request.addValue("Bearer \(CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.oAuthToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        session.dataTask(with: request) { (data, response, error) in
            
            if data != nil {
                guard let response = data else { completionHandler(nil,"Erro ao retornar dados"); return }
                guard let parsedStatus = try? JSONDecoder().decode(StatusResponse.self, from: response) else { completionHandler(nil,"Erro ao retornar dados"); return }
                
                if parsedStatus.status == "PROCESSING" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        TbdQdo2WxXg2mn5u1nvpMpmoWnvoT1L8(transaction: transaction, completionHandler: completionHandler)
                        print("PROCESSING")
                    }
                } else if parsedStatus.status == "COMPLETE" {
                    print("COMPLETE")
                    completionHandler(data,nil)
                } else {
                    print("Error")
                    completionHandler(nil,"Erro ao retornar dados")
                }
                
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    public class func postEnroll (completionHandler: @escaping onCompletionData){
        guard let urlComp = URLComponents(string: defaultURL + "core-op/gis") else { return }
        let session = u7cnXd4PS4n6whTd2BzlogYU0k6diM2A()
        var request = URLRequest(url: urlComp.url!)
        
        request.httpMethod = "POST"
        request.addValue("Bearer \(CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.oAuthToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let body = CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.HkAhF9UMSNjvovjcKGnCDf7JWsvlccqK()
        
        let jsonBody = try? JSONEncoder().encode(body)
        request.httpBody = Data(jsonBody!)
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    private class func X3FTuM30zSY69kmARFQYowoaJsVjreeI(statusCode: Int?, data: Data?, error: Error?, onCompletion: @escaping (_ handleResponse: e3HD3w71nt1gBqbmcijIjY8hfemI4CHJ) -> Void) {
        if data != nil {
            if statusCode! >= 200 && statusCode! < 300 {
                onCompletion(.HTGIrlvHdeNhr3nAL8HOlMInCBVoS5v1)
            } else if statusCode == 401 || statusCode == 403 {
                
                onCompletion(.Qb92cxlXl8pVGdjgpMQcnR9aTArfLSB8)
            } else {
                onCompletion(.vu2xsd9F3agXSgQt7wispKZolmr54iCj)
            }
        } else if error != nil {
            if (error! as NSError).code == XvozoHUyzczmfv8HOtOG8U8adBWwvSgH.vji1dJ8gOgbHg1uP2hUWINeuFOyLGHlE.rawValue {
                onCompletion(.Qb92cxlXl8pVGdjgpMQcnR9aTArfLSB8)
            } else {
                onCompletion(.Hku2p1hlQu5kVtirStF9yqA9JReXxwhA)
            }
        } else {
            onCompletion(.Hku2p1hlQu5kVtirStF9yqA9JReXxwhA)
        }
    }
}

//TODO: Need Refactor
extension fBBdSIwH95GqQOD8PcbWxh8VLutb4MA2 {
    private class func XIRFsNufBin8zdBYAPV3nCl8Vi3NA9ff(boundary: String, username: String, password: String) -> Data{
        
        let parameters = [
            [
                "key": "grant_type",
                "value": "password",
                "type": "text"
            ],
            [
                "key": "username",
                "value": username,
                "type": "text"
            ],
            [
                "key": "password",
                "value": password,
                "type": "text"
            ]] as [[String : Any]]
        
        var body = ""
        //        var error: Error? = nil
        for param in parameters {
            if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] as! String
                if paramType == "text" {
                    let paramValue = param["value"] as! String
                    body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                    let paramSrc = param["src"] as! String
                    let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                        + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
            }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        return postData!
    }
}
