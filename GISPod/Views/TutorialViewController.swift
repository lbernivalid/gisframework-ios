//
//  TutorialViewController.swift
//  GISPod
//
//  Created by VALID Certificadora on 30/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import UIKit

class TutorialViewController: HyTNha3KZ0cdlFeqJgXLJpwiv55Ccl8o {
    
    @IBAction func continueBtn(_ sender: Any) {
        let frameworkBundle = Bundle(for: LivenessViewController.self)
        let liveVC = LivenessViewController(nibName: "LivenessViewController", bundle: frameworkBundle)
        self.navigationController?.pushViewController(liveVC, animated: true)
    }
}
