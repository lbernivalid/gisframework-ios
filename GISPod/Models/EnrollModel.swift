//
//  EnrollModel.swift
//  GISPod
//
//  Created by VALID Certificadora on 20/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

struct EQFQtWq3j7mxyrgF2kzFcsnTanMzAgnc: Codable {
    var contractItem: String
    var biometricData: pLOcSeuK2jsxN13XioMHWUG2F5RKl0de
    var documents: [EVWtDBsZZch41sb7sLgCIb9vNtdkveCn]
    var faceBase64: String?
    var documentBase64: String?
    var livenessScore: Double?
    var customFields: String?
    var name: String?
    var birthDate: String? //Ano/mes/dia
    var filiation: MOo6idt3CfJqAWRTA7ub8VeCQFFh51XY?
}

struct ndgEOfdTz0o5sFlGxz4yaaJT2q0OjdO1: Codable {
    var transaction: Int
    var callbackConsulta: String
    var suggestedCallbackCallInMinutes: Int
}

