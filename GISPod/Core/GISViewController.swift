//
//  GISViewController.swift
//  GISPod
//
//  Created by VALID Certificadora on 03/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation
import UIKit

class HyTNha3KZ0cdlFeqJgXLJpwiv55Ccl8o: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let backImg = UIImage(named: "ic.back", in: Bundle(for: HyTNha3KZ0cdlFeqJgXLJpwiv55Ccl8o.self), compatibleWith: nil)
        let closeButton = UIBarButtonItem(title: " ", style: .plain, target: self, action: #selector(EPOyF8ATZfhWJrXEnycqH5PApTN3yWy5(_:)))
        
        closeButton.setBackgroundImage(backImg, for: .normal, barMetrics: .default)
        navigationItem.leftBarButtonItem = closeButton
        self.navigationController?.navigationBar.backItem?.title = " "
        self.navigationController?.navigationBar.tintColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.e6KCrZARWH7GOtuxp4pnbuDwpZtX1vTt), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.R53rOff40wouOsLSocYQEUPs8HYsZ92K), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.qA03wZGojOEgNEwgpZPpOsTDR8tDvEat))
        view.addGestureRecognizer(tap)
    }
    
    @objc func qA03wZGojOEgNEwgpZPpOsTDR8tDvEat() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func EPOyF8ATZfhWJrXEnycqH5PApTN3yWy5(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Atenção!", message: "Operação cancelada. Para consultar seu score, por favor, siga todas as instruções.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Fechar", style: .default, handler: { (action) in
                CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController?.dismiss(animated: true, completion: nil)
            }))
            CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController?.navigationController?.popToRootViewController(animated: true)
            CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Keyboard functions
    
    // Move the view up incase the keyboard overlaps the textfield
    @objc func e6KCrZARWH7GOtuxp4pnbuDwpZtX1vTt(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardFrame = keyboardSize.cgRectValue.height
        
        let selectedTextView = view.cO2mJBNYE68SijaAkTCgQi7A9jHNzwim()
        
        guard let bottonPosition = selectedTextView?.superview?.convert((selectedTextView?.frame.origin)!, to: nil).y else { return }
        
        let screenHeight = UIScreen.main.bounds.height - bottonPosition
        
        if (bottonPosition + (selectedTextView?.frame.height)!) > keyboardSize.cgRectValue.minY {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= (keyboardFrame - screenHeight + 5 + (selectedTextView?.frame.height)!)
            }
        }
        
    }
    
    // Make the view return to the center point of the screen, after the keyboard hides
    @objc func R53rOff40wouOsLSocYQEUPs8HYsZ92K(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            view.center = CGPoint(x:
                UIScreen.main.bounds.midX, y:
                UIScreen.main.bounds.midY)
        }
    }
}
