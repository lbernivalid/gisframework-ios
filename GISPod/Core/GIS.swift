//
//  GIS.swift
//  GISPod
//
//  Created by VALID Certificadora on 18/03/20.
//

import Foundation
import UIKit

public protocol GISDelegate: class {
    func callbackStatus(_ status: StatusResponse?)
}

public class GIS: NSObject {
    public class func start(viewController: UIViewController, delegate: GISDelegate, operations: [GISOperation], requestCode: Int, extrafields: [[GISOperation: [GISField]]]) {
        //call TODO: Call views of operations and set enroll by the end of the flow
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.operations = operations
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController = viewController
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.extraFields = extrafields
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.delegate = delegate
        nF3h6eXbjMliVEBjyFJsbabguW9ISjQK.OKCX84wdgylYmT6Ads5HzEh3NehTko39()
        
    }
    
    public class func initGIS(username: String, password: String, completionHandler: @escaping onCompletion) {
        nF3h6eXbjMliVEBjyFJsbabguW9ISjQK.TT8chXOpwMUFXc2F2DDP5yXuxYeMOMq1(username: username, password: password) { (success, error) in
            completionHandler(success, error)
        }
    }
}
