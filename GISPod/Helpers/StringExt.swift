import Foundation
import UIKit

extension String {
    //: ### Base64 encoding a string
    func M9nDBUaxLWTlAdcSxjUAYPUO3FCU1rA9() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    func lbDQItpF1BIsmvBJwHaZEG7vUDOV3ZiK() -> String? {
        let unreserved = "-._~?"
        let allowedCharacterSet = NSMutableCharacterSet.alphanumeric()
        allowedCharacterSet.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowedCharacterSet as CharacterSet)
    }
    
    func nlVixkBBuM29z6prMJ2Wezjmi4YMYC4S() -> String? {
        let unreserved = "-._~? /"
        let allowedCharacterSet = NSMutableCharacterSet.alphanumeric()
        allowedCharacterSet.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowedCharacterSet as CharacterSet)
    }
    
    func N7Tjv3SECiiyjhXIVZh4xj2vSKufxuZQ(theInt: Int)-> String.Index {
        return index(self.startIndex, offsetBy: theInt)
    }
    
    func OIEhN2XZ7ZX6aVRBBBI1obGXMF7n6uqx(for regexPattern: String) -> [[String?]?]? {
        do {
            let text = self
            let regex = try NSRegularExpression(pattern: regexPattern)
            let matches = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return matches.map { match in
                return (0..<match.numberOfRanges).map {
                    let rangeBounds = match.range(at: $0)
                    guard let range = Range(rangeBounds, in: text) else {
                        return ""
                    }
                    return String(text[range])
                }
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    //: ### Base64 decoding a string
    func pTSJP0xCDm651NkCi1YeGDHLeca1GqtG() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    func lY8p5w0o4kmSOnhqdfaAE2FLD636QRKN() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .ascii)
        }
        return nil
    }
    
    func qUMv7S1hdTnkNUBDWpoLWFD8uafS0uLN() -> Bool {
        guard Data(base64Encoded: self) != nil else {
            return false
        }
        return self.count > 50
    }
    
    func stdCfnIwKymjCSKBYsnZBKjpKTSNQlT6() -> [String: String]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print("erro: \(error.localizedDescription)")
            }
        } else if let data = self.data(using: .ascii) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print("erro: \(error.localizedDescription)")
            }
        }
        return nil
    }
    
    func LAPqlVcXqtn14AQaNdRWQNQvyu5HkFvn() -> String {
        var characters = [Character](self)
        
        characters.insert(".", at: 3)
        characters.insert(".", at: 7)
        characters.insert("-", at: 11)
        
        return String(characters)
    }
    
    // The first part between @ is given a color / The first part between * is given bold text - Both parts are using roboto Medium as font
    func FRuJyfZALL8RAIE8HOSB8rwMgt862yEn() -> NSMutableAttributedString {
        
        let formatedMessage = NSMutableAttributedString(string: self)
        
        //  Format the part between @
        if let openArrobaRange = self.range(of: "@"),
            let closeArrobaRange = self.range(of: "@", range: openArrobaRange.upperBound..<self.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3123499751, green: 0.2975655198, blue: 0.5166683197, alpha: 1), NSAttributedString.Key.font: UIFont.VkVsgovoB2PUmFHenU7jC2UnSGyhAc3f(ofSize: 24)], range: NSRange(openArrobaRange.upperBound..<closeArrobaRange.lowerBound, in: self))
        } else {
            NSLog("Could not get string range between @")
        }
        
        // Format the part between *
        if let openAsteriskRange = self.range(of: "*"),
            let closeAsteriskRange = self.range(of: "*", range: openAsteriskRange.upperBound..<self.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.font: UIFont.VkVsgovoB2PUmFHenU7jC2UnSGyhAc3f(ofSize: 8)], range: NSRange(openAsteriskRange.upperBound..<closeAsteriskRange.lowerBound, in: self))
        } else {
            NSLog("Could not get string range between *")
        }
        
        // Format the part between #
        if let openHashtagRange = self.range(of: "#"),
            let closeHashtagRange = self.range(of: "#", range: openHashtagRange.upperBound..<self.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4669571519, green: 0.5273934603, blue: 0.6115893126, alpha: 1), NSAttributedString.Key.font: UIFont.uvBnLVLPutL5chlgh8I3zy7KqaOBfkpS(ofSize: 14)], range: NSRange(openHashtagRange.upperBound..<closeHashtagRange.lowerBound, in: self))
        } else {
            NSLog("Could not get string range between *")
        }
        
        formatedMessage.mutableString.replaceOccurrences(of: "@", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        formatedMessage.mutableString.replaceOccurrences(of: "*", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        formatedMessage.mutableString.replaceOccurrences(of: "#", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        
        return formatedMessage
    }
}

extension NSMutableAttributedString {
    func OWPVsRUb4MWvLeO5mPrSow0NGZbVGhVl(fragment: String, color: UIColor, fontSize: CGFloat = 22) -> NSMutableAttributedString {
        let content = self.string
        let range = NSString(string: content).range(of: fragment)
        self.addAttributes([.foregroundColor: color, .font: UIFont.boldSystemFont(ofSize: fontSize)], range: range)
        return self
    }
}

extension UIFont {
    class func yzvHKvDcaBmyhPzSWSp29SEh4o8ObvY0(ofSize size: CGFloat) -> UIFont {
        guard let regular = UIFont(name: "Rubik-Regular", size: size) else { return UIFont(name: "TimesNewRomanPSMT", size: size)! }
        return regular
    }
    class func ZHujnSBc575tTOtRZvoygMHhDjgpUYTc(ofSize size: CGFloat) -> UIFont {
        guard let light = UIFont(name: "Rubik-Light", size: size) else { return UIFont(name: "TimesNewRomanPSMT", size: size)! }
        return light
    }
    class func VkVsgovoB2PUmFHenU7jC2UnSGyhAc3f(ofSize size: CGFloat) -> UIFont {
        guard let medium = UIFont(name: "Rubik-Medium", size: size) else { return UIFont(name: "TimesNewRomanPSMT", size: size)! }
        return medium
    }
    class func uvBnLVLPutL5chlgh8I3zy7KqaOBfkpS(ofSize size: CGFloat) -> UIFont {
        guard let bold = UIFont(name: "Rubik-Bold", size: size) else { return UIFont(name: "TimesNewRomanPSMT", size: size)! }
        return bold
    }
    
    // Escala a fonte com base na altura do storyboard utilizado (iphone 11)
    class func lkfu4yodSqotfqAS70G7Xx03CAydXK9h(fontSize: CGFloat) -> CGFloat {
         return (fontSize/808)*(UIScreen.main.bounds.height)
     }
}
