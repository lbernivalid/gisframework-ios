//
//  GISAlert.swift
//  GISPod
//
//  Created by VALID Certificadora on 09/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation
import UIKit

public class GISAlert {
    
    public static func RwfSdXryzbn6K3cjexF02vMt2pjckyAq(
        title: String,
        message: String?,
        actions: [UIAlertAction],
        style: UIAlertController.Style = .alert,
        in vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            alert.addAction(action)
        }
        DispatchQueue.main.async {
            vc.present(alert, animated: true, completion: nil)
        }
        
    }
    
    public static func WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: String, message: String, actionTitle: String, vc: UIViewController) {
        GISAlert.RwfSdXryzbn6K3cjexF02vMt2pjckyAq(title: title, message: message, actions:
            [UIAlertAction(title: actionTitle, style: .default, handler: nil)], in: vc)
    }
}
