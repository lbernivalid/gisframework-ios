//
//  ViewController.swift
//  POCAVFoundation
//
//  Created by VALID Certificadora on 02/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import UIKit
import AVFoundation

class LivenessViewController: HyTNha3KZ0cdlFeqJgXLJpwiv55Ccl8o, AVCapturePhotoCaptureDelegate {
    @IBOutlet var previewView: UIView!
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        captureSession = AVCaptureSession()
        stillImageOutput = AVCapturePhotoOutput()
        
        captureSession.sessionPreset = .medium
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true
                    else {
                        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
                        DispatchQueue.main.async {
                            self.navigationController?.popToRootViewController(animated: true)
                            GISAlert.RwfSdXryzbn6K3cjexF02vMt2pjckyAq(title: "Atenção", message: "GIS precisa de acesso à câmera", actions: [UIAlertAction(title: "Permitir Camera", style: .default, handler: { (alert) in
                                DispatchQueue.main.async {
                                    UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
                                }
                            })], in: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController!)
                            
                        }
                        
                        return }
                guard let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }
                DispatchQueue.main.async {
                    do {
                        let input = try AVCaptureDeviceInput(device: frontCamera)
                        if self.captureSession.canAddInput(input) && self.captureSession.canAddOutput(self.stillImageOutput) {
                            self.captureSession.addInput(input)
                            self.captureSession.addOutput(self.stillImageOutput)
                            self.xjEfMAyFa8mJdtaTk3vo4TtNBSDaR65w()
                        }
                    }
                    catch let error  {
                        print("Error Unable to initialize back camera:  \(error.localizedDescription)")
                    }
                }
                
            })
        case .restricted:
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            GISAlert.RwfSdXryzbn6K3cjexF02vMt2pjckyAq(title: "Atenção", message: "GIS precisa de acesso à câmera", actions: [UIAlertAction(title: "Permitir Camera", style: .default, handler: { (alert) in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            })], in: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController!)
            self.navigationController?.popToRootViewController(animated: true)
        case .denied:
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            GISAlert.RwfSdXryzbn6K3cjexF02vMt2pjckyAq(title: "Atenção", message: "GIS precisa de acesso à câmera", actions: [UIAlertAction(title: "Permitir Camera", style: .default, handler: { (alert) in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            })], in: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController!)
            self.navigationController?.popToRootViewController(animated: true)
        case .authorized:
            guard let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }
            do {
                let input = try AVCaptureDeviceInput(device: frontCamera)
                if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                    captureSession.addInput(input)
                    captureSession.addOutput(stillImageOutput)
                    xjEfMAyFa8mJdtaTk3vo4TtNBSDaR65w()
                }
            }
            catch let error  {
                print("Error Unable to initialize back camera:  \(error.localizedDescription)")
            }
        @unknown default:
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        
    }
    
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        //        navigationController?.navigationBar.isHidden = true
    //        view.subviews.forEach({ view.bringSubviewToFront($0) })
    //        if !captureSession.isRunning {
    //            captureSession.startRunning()
    //        }
    //    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    func xjEfMAyFa8mJdtaTk3vo4TtNBSDaR65w() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.frame = view.layer.bounds
        videoPreviewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(videoPreviewLayer)
        captureSession.startRunning()
        
        view.subviews.forEach({ view.bringSubviewToFront($0) })
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.operations?.removeFirst()
        nF3h6eXbjMliVEBjyFJsbabguW9ISjQK.OKCX84wdgylYmT6Ads5HzEh3NehTko39()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard var imageData = photo.fileDataRepresentation() else { return }
        guard var image = UIImage(data: imageData) else { return }
        image = image.TKU72Ie8wePC48AFcfBHNlZs9x36cur1()!
        image = image.BQuap8yZTJnZosaKM1oDzeu1kA3obeKz(kb: 300)!
        imageData = image.pngData()!
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.imageData = imageData
    }
    
}

