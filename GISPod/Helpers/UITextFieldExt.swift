//
//  UITextFieldExt.swift
//  GISPod
//
//  Created by VALID Certificadora on 09/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    public func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(X4LS0RdHpocNIjWxl45dBlc7FUy1yYRe))
        let onDone = onDone ?? (target: self, action: #selector(U4TumbEtVbF3l4dgkkvX3Yebi07RqOYG))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }

    // Default actions:
    @objc func U4TumbEtVbF3l4dgkkvX3Yebi07RqOYG() {
        self.resignFirstResponder()
//        UITextFieldDelegate.textFieldShouldReturn?(self.superview as! UITextFieldDelegate)
    }
    @objc func X4LS0RdHpocNIjWxl45dBlc7FUy1yYRe() { self.resignFirstResponder() }
}
