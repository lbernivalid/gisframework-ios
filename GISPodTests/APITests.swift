//
//  APITests.swift
//  GISPodTests
//
//  Created by VALID Certificadora on 23/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

internal class APITests: NSObject {
    public typealias onCompletion = ((Bool, String?) -> Void)
    public typealias onCompletionData = ((Data?, String?) -> Void)
    
    
    private enum CODES: Int {
        case UPDATED_NEEDED = -1
        case AUTH_NEEDED = -1206
    }
    
    private enum HandleResponse {
        case success
        case retry
        case genericError
        case parsedError
    }
    
    static let defaultURL = "https://core-hml.validbio.com.br/"
    static var basicAuth = ""
    
    internal class func authenticate(username: String, password: String, onCompletion: @escaping onCompletion) {
        postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
            postInfo(token: json!.access_token) { (data, error) in
                let info = try? JSONDecoder().decode(InfoResponse.self, from: data!)
                getRules(contractUUID: (info?.listOfContractProductInfos.first!.contractUuid)!, accessToken: json!.access_token) { (success, error) in
                    _ = try? JSONDecoder().decode(RulesResponse.self, from: data!)
                    onCompletion(true, "")
                }
            }
        }
    }
    
    
    class func pinSession () -> URLSession {
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 15.0
        sessionConfig.timeoutIntervalForResource = 300.0
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        let username = "validbio-oauth2-client"
        let password = "validbio-oauth2-client-clivalidbio"
        let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
        basicAuth = loginData.base64EncodedString()
        
        return session
    }
    
    class func postToken (username: String, password: String, completionHandler: @escaping onCompletionData) {
        guard let url = URL(string: defaultURL + "oauth/token") else { return }
        let session = pinSession()
        var request = URLRequest(url: url)
        let boundary = "Boundary-\(UUID().uuidString)"
        
        request.httpBody = postmanFormData(boundary: boundary, username: username, password: password)
        request.httpMethod = "POST"
        request.addValue("Basic \(basicAuth)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=--------------------------441134357055693654586910", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
        
    }
    
    class func postmanFormData(boundary: String, username: String, password: String) -> Data{
        
        let parameters = [
            [
                "key": "grant_type",
                "value": "password",
                "type": "text"
            ],
            [
                "key": "username",
                "value": username,
                "type": "text"
            ],
            [
                "key": "password",
                "value": password,
                "type": "text"
            ]] as [[String : Any]]
        
        var body = ""
//        var error: Error? = nil
        for param in parameters {
            if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] as! String
                if paramType == "text" {
                    let paramValue = param["value"] as! String
                    body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                    let paramSrc = param["src"] as! String
                    let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                        + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
            }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        return postData!
    }
    
    class func postInfo (token: String, completionHandler: @escaping onCompletionData) {
        guard let url = URL(string: defaultURL + "info") else { return }
        let session = pinSession()
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    class func getRules (contractUUID: String, accessToken: String, completionHandler: @escaping onCompletionData){
        guard let url = URL(string: defaultURL + "rule-manager/rules/\(contractUUID)/GIS_ONBOARDING") else { return }
        let session = pinSession()
        var request = URLRequest(url: url)
        
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    class func postEnroll (accessToken: String, completionHandler: @escaping onCompletionData){
        guard let urlComp = URLComponents(string: defaultURL + "core-op/gis") else { return }
        let session = pinSession()
        var request = URLRequest(url: urlComp.url!)
        request.httpMethod = "POST"
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
//        let body = EnrollRequest(contractItem: "3ec15407-64f9-4573-a12a-26390414b8de",
//                                 biometricData: BiometricData(face: "{{face}}", fingerPrints: []),
//                                 documents: [DocumentBio(docType: "CPF", document: "12345678900")],
//                                 customFields: "error")
//        let jsonBody = try? JSONEncoder().encode(body)
//        request.httpBody = Data(jsonBody!)
        
        session.dataTask(with: request) { (data, response, error) in
            if data != nil {
                completionHandler(data,nil)
            } else {
                completionHandler(nil,"Erro ao retornar dados")
            }
        }.resume()
    }
    
    private class func handleResponse(statusCode: Int?, data: Data?, error: Error?, onCompletion: @escaping (_ handleResponse: HandleResponse) -> Void) {
        if data != nil {
            if statusCode! >= 200 && statusCode! < 300 {
                onCompletion(.success)
            } else if statusCode == 401 || statusCode == 403 {
                
                onCompletion(.retry)
            } else {
                onCompletion(.parsedError)
            }
        } else if error != nil {
            if (error! as NSError).code == CODES.AUTH_NEEDED.rawValue {
                onCompletion(.retry)
            } else {
                onCompletion(.genericError)
            }
        } else {
            onCompletion(.genericError)
        }
    }
}
