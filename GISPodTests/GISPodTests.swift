//
//  GISPodTests.swift
//  GISPodTests
//
//  Created by VALID Certificadora on 23/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import XCTest
@testable import GISPod

class GISPodTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testConversion() {
        //        let test = "0.9878"
        //        let converted = Int(Double(test)!*1000)
        //        print(converted)
        
        var formatCPF = "051.742.378-25"
        formatCPF = formatCPF.replacingOccurrences(of: ".", with: "")
        formatCPF =  formatCPF.replacingOccurrences(of: "-", with: "")
        let docBio = DocumentBio(docType: "CPF", document: formatCPF)
        
        prinTest(string: docBio)
    }
    
    func testToken() {
        let expectation = XCTestExpectation(description: "Download token")
        APITests.postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
            XCTAssertNotNil(json, "All Good")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 100.0)
    }
    
    func testInfo() {
        let expectation = XCTestExpectation(description: "Download token")
        APITests.postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
            APITests.postInfo(token: json!.access_token, completionHandler: {(data, error) in
                XCTAssertNotNil(data, "All Good")
                expectation.fulfill()
            })
            
        }
        wait(for: [expectation], timeout: 100.0)
    }
    
//    func testEnroll() {
//        let expectation = XCTestExpectation(description: "Download token")
//        
//        APITests.postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
//            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
//            GISStorage.oAuthToken = json?.access_token
//            GISStorage.imageData = Data(base64Encoded: "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==")
//            APIClient.postEnroll() { (data, error) in
//                let json = try? JSONDecoder().decode(EnrollResponse.self, from: data!)
//                XCTAssertNotNil(json, "Not Good")
//                expectation.fulfill()
//            }
//        }
//        wait(for: [expectation], timeout: 500.0)
//    }
    
    func testStatus() {
        let expectation = XCTestExpectation(description: "Download token")
        
        APITests.postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
            GISStorage.oAuthToken = json?.access_token
            APIClient.getStatus(transaction: "10425") { (data, error) in
                let json = try? JSONDecoder().decode(StatusResponse.self, from: data!)
                
                guard let details = json?.data?.scoreDetail.biographic else { return }
                let infoDetails = details[0].information.details
                for item in infoDetails {
                    
                    if item.name == "Data de Nascimento" {
                        let splited = item.value.split(separator: "-")
                        self.prinTest(string:"Data de nascimento: \(splited[2])/\(splited[1])/\(splited[0])")
                    } else {
                        
                        let intScore = Int(Double(item.score)!)
                        
                        self.prinTest(string:"\(item.name): \(intScore)")
                    }
                }
                
                
                XCTAssertNotNil(json, "Not Good")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 500.0)
    }
    
    func testAuthenticate() {
        let expectation = XCTestExpectation(description: "Authentication Flow")
//        APITests.postToken(username: "validbio-oauth2-client", password: "validbio-oauth2-client-clivalidbio") { (data, error) in
        APITests.postToken(username: "37223427833", password: "rkLAunEO") { (data, error) in
            let json = try? JSONDecoder().decode(OAuthRequest.self, from: data!)
            GISStorage.oAuthToken = json!.access_token
            APITests.postInfo(token: GISStorage.oAuthToken!) { (data, error) in
                let info = try? JSONDecoder().decode(InfoResponse.self, from: data!)
                GISStorage.contractUUID = info?.listOfContractProductInfos.first?.contractUuid
                APITests.getRules(contractUUID: GISStorage.contractUUID!, accessToken: GISStorage.oAuthToken!) { ( rule, error) in
                    let rules = try? JSONDecoder().decode(RulesResponse.self, from: rule!)
                    XCTAssertNotNil(rules, "Not Good")
                    expectation.fulfill()
                }
            }
        }
        
        wait(for: [expectation], timeout: 100.0)
    }
    
    func testAuthenticateOF() {
        let expectation = XCTestExpectation(description: "Authentication Flow")
        APIClient.authenticate(username: "37223427833", password: "rkLAunEO") { (success, error) in
            XCTAssertTrue(success)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 100.0)
    }
    
    func testGISFieldOperation() {
        let dict = [[GISOperation.CUSTOM_FIELDS: [GISField.CPF()]],
                    [GISOperation.CUSTOM_FIELDS: [GISField.FULL_NAME(), GISField.FIRST_NAME(), GISField.RG()]]]
        let first = dict.first?[GISOperation.CUSTOM_FIELDS]
        for item in first! {
            prinTest(string: item)
        }
    }
    
    func prinTest(string: Any) {
        print("@Tests \(string)")
    }
}
