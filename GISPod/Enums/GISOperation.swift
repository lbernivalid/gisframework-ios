//
//  GisOperation.swift
//  GISPod
//
//  Created by VALID Certificadora on 18/03/20.
//

import Foundation

public enum GISOperation {
    case LIVENESS, OCR_CNH, MATCH_USER_WITH_CNH, CUSTOM_FIELDS
}
