//
//  ServicesProvider.swift
//  GISPod
//
//  Created by VALID Certificadora on 31/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

public enum ServicesProvider: String, Codable {
    case FACETEC, DOT, MEERKAT
}
