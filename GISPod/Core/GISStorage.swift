//
//  GISStorage.swift
//  GISPod
//
//  Created by VALID Certificadora on 31/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation
import UIKit

class CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6 {
    //Setup
    static var oAuthToken: String?
    static var contractUUID: String?
    static var configs: ZEcwwGJPpbQ63JmW7F13e2m5rjNaABBY?
    static var delegate: GISDelegate?
    
    //Engine Holders
    static var operations: [GISOperation]?
    static var viewController: UIViewController?
    static var extraFields: [[GISOperation: [GISField]]] = []
    
    //Enroll Holders
    static var imageData: Data?
    static var docBioArray: [EVWtDBsZZch41sb7sLgCIb9vNtdkveCn] = []
    static var transactionID: Int?
    
    class func PE3gcNn2FuJw9vqMsq4P6momQpPZWskC() {
        oAuthToken = nil
        contractUUID = nil
        configs = nil
        imageData = nil
        docBioArray = []
        transactionID = nil
    }
    
    
    class func HkAhF9UMSNjvovjcKGnCDf7JWsvlccqK() -> EQFQtWq3j7mxyrgF2kzFcsnTanMzAgnc {
        let bioData = pLOcSeuK2jsxN13XioMHWUG2F5RKl0de.init(face: (imageData?.base64EncodedString())!, fingerPrints: [])
        let filiation = MOo6idt3CfJqAWRTA7ub8VeCQFFh51XY(fatherName: "", motherName: "")
        var enroll = EQFQtWq3j7mxyrgF2kzFcsnTanMzAgnc(contractItem: CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.contractUUID!, biometricData: bioData, documents: [], faceBase64: "", documentBase64: "", livenessScore: nil, customFields: nil, name: "", birthDate: "", filiation: filiation)
        for item in self.docBioArray {
            if item.docType == FieldType.CPF.rawValue {
                var formatCPF = item.document
                formatCPF = formatCPF.replacingOccurrences(of: ".", with: "")
                formatCPF =  formatCPF.replacingOccurrences(of: "-", with: "")
                let docBio = EVWtDBsZZch41sb7sLgCIb9vNtdkveCn(docType: item.docType, document: formatCPF)
                enroll.documents.append(docBio)
            } else if item.docType == FieldType.FIRST_NAME.rawValue || item.docType == FieldType.FULL_NAME.rawValue {
                enroll.name = item.document
            } else if item.docType == FieldType.MOTHERS_NAME.rawValue {
                enroll.filiation?.motherName = item.document
            } else if item.docType == FieldType.BIRTHDATE.rawValue {
                let dateSplit = item.document.split(separator: "/")
                if dateSplit.count == 3 {
                    let formatDate = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0]
                    enroll.birthDate = formatDate
                } else {
                    enroll.birthDate = ""
                }
            }
        }
        
        return enroll
    }
}
