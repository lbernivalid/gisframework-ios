//
//  InfoModel.swift
//  GISPod
//
//  Created by VALID Certificadora on 20/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

struct GtvctEg08DdhQwPIGZ3SzKyDy85gzuju: Codable {
    var errorCode: Int
    var errorMessage: String?
    var valid: Bool
    var listOfContractProductInfos: [V46264yn4n2UmQ7mwWH8C68SfGk5V1px]
    
    func cy2Va7jRflmsnCQrUA4mUCNXraxp4xvl() -> Bool {
        return ((errorMessage == nil || errorMessage == "")  && valid && listOfContractProductInfos.count > 0)
    }
}
