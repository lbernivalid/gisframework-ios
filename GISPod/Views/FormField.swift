//
//  FormField.swift
//  GISPod
//
//  Created by VALID Certificadora on 06/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import UIKit

class FormField: UIView {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var formField: SwiftMaskTextfield!
    
    class func lIV9VtO1zvI4wtrCk9wczXJouZWDtGbo(title: String, placeholder: String) -> FormField {
        let frameworkBundle = Bundle(for: TutorialViewController.self)
        let formView = UINib(nibName: "FormField", bundle: frameworkBundle).instantiate(withOwner: self, options: nil)[0] as! FormField
        formView.YiW1sQ25qVl4x6s4HYPjLNNz2Us5Tskd(title: title, placeholder: placeholder)
        
        return formView
    }
    
    private func YiW1sQ25qVl4x6s4HYPjLNNz2Us5Tskd(title: String, placeholder: String) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.formField.frame.height))
        formField.leftView = paddingView
        formField.leftViewMode = UITextField.ViewMode.always
        formField.placeholder = placeholder
        titleLabel.text = title
    }
    
}
