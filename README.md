# GIS SDK Core (Needs Update)
Core app of the VIDaaS project, this app is used to create most of the app we will be publishing and selling like RG / CFM and so on.

## Getting Started

Clone the project
Install CocoaPods
Select any scheme for the environment you wish to use
Build / Run

We usually make branches from develop to make new features and only merge on master the builds that goes into production

## Deployment

Production deploys are manual, archiving the project on the desired target and Prod scheme
QA deploys are automated and you should only do them from the branch develop
To deploy to QA simply put the following tag on the commit in develop to export a builds:
    GIS Demo -> v_gis_hml_*

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Secret

Obfuscation String -> 6787974078 


