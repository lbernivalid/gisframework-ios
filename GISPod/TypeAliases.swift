//
//  TypeAliases.swift
//  GISPod
//
//  Created by VALID Certificadora on 19/03/20.
//

import Foundation
public typealias onCompletion = ((Bool, String?) -> Void)
public typealias onCompletionData = ((Data?, String?) -> Void)
