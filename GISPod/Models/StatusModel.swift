//
//  StatusModel.swift
//  GISPod
//
//  Created by VALID Certificadora on 09/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

public struct StatusResponse: Codable {
    public var message: String
    public  var status: String
    public var data: StatusData?
}

public struct StatusData: Codable {
    public var status: Int?
    public var message: String?
    public var transaction: String?
    public var operation: String?
    public var score: String? // 1/1
    public var scoreDetail: ScoreDetail
}

public struct ScoreDetail: Codable {
    public var biographic: [ScoreDetailItem]
    public var biometric: [ScoreDetailItem]
}


public struct ScoreDetailItem: Codable {
    public var agreementId: String
    public var agreementName: String
    public var information: ScoreInformation
    public var errors: [ScoreError]
}

public struct ScoreInformation: Codable {
    public var scoreWeight: Int
    public var status: Int
    public var message: String
    public var gallery: String?
    public var name: String
    public var birthDate: String
    public var details: [DetailedItemWithScore]
}

public struct DetailedItemWithScore: Codable {
    public var name: String
    public var value: String
    public var score: String
}

public struct ScoreError: Codable {
    public var message: String
    public var name: String
}
