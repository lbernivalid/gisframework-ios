Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "GISPod"
s.summary = "GISPod is the pod to be used as a core/engine for the GISProject"
s.requires_arc = true

# 2
s.version = "0.1.81"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Leon Berni" => "leon.berni@valid.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/validcertificadora/gis-ios"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "git@bitbucket.org:lbernivalid/gisframework-ios.git",
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"

# 8
s.source_files = "GISPod/**/*.{swift}"
# s.resources = "GISPod/**/*.{xib}"

# 9
s.resources = "GISPod/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "5.0"

end
