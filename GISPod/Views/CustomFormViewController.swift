//
//  CustomFormViewController.swift
//  GISPod
//
//  Created by VALID Certificadora on 06/04/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import UIKit

class CustomFormViewController: HyTNha3KZ0cdlFeqJgXLJpwiv55Ccl8o {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var continueBtn: UIButton!
    //    let stackView = UIStackView()
    var customFields: [GISField] = []
    var textFields: [SwiftMaskTextfield] = []
    var bioDocArray: [EVWtDBsZZch41sb7sLgCIb9vNtdkveCn] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        xFi5tcAeqws7E6qBPiqrY5kiWlaKUxY5()
        if CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.extraFields.count == 1 {
            continueBtn.setTitle("Consultar Score", for: .normal)
        } else {
            continueBtn.setTitle("Continuar", for: .normal)
        }
    }
    
    func l8rJomFOsEnjqZ2BIEXdFGSzAZ9FqhNh(index: Int) -> Bool{
        if textFields[index].text == "" && customFields[index].required == true {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Operação cancelada. Para consultar seu score, por favor, siga todas as instruções.", actionTitle: "OK", vc: self)
            return false
        }
        
        if (textFields[index].text!.count < customFields[index].mask.count && customFields[index].required == true) || (textFields[index].text != "" && textFields[index].text!.count < customFields[index].mask.count){
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Campo inválido", actionTitle: "OK", vc: self)
            return false
        }
        
        if (customFields[index].type == FieldType.BIRTHDATE && customFields[index].required == true) || (textFields[index].text != "" &&  customFields[index].type == FieldType.BIRTHDATE){
            return b10eKVVnC2RgiXFN0cwYKYwyJMVnA970(index: index)
        }
        
        return true
    }
    
    func b10eKVVnC2RgiXFN0cwYKYwyJMVnA970(index: Int) -> Bool {
        let date = textFields[index].text?.split(separator: "/")
        if Int(String(date![1]))! > 12 {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Mês inválido", actionTitle: "OK", vc: self)
            return false
        }
        
        if Int(String(date![1]))!%2 == 0 && Int(String(date![1]))! <= 7  {
            if Int(String(date![1]))! == 2 {
                if Int(String(date![2]))!%4 == 0 && Int(String(date![0]))! > 29 {
                    GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
                    return false
                } else if Int(String(date![0]))! > 28 {
                    GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
                    return false
                }
            } else if Int(String(date![0]))! > 30 {
                GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
                return false
            }
        } else if Int(String(date![1]))!%2 == 1 && Int(String(date![0]))! > 31 && Int(String(date![1]))! <= 7 {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
            return false
        } else if Int(String(date![1]))! > 7 && Int(String(date![1]))!%2 == 0 && Int(String(date![0]))! > 31 {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
            return false
        } else if Int(String(date![1]))! > 7 && Int(String(date![1]))!%2 == 1 && Int(String(date![0]))! > 30 {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Dia inválido", actionTitle: "OK", vc: self)
            return false
        }
        
        if Int(String(date![2]))! < 1888 {
            GISAlert.WrmeVvhpAPurn4Snbj58bdD812sPmmpp(title: "Atenção", message: "Ano inválido", actionTitle: "OK", vc: self)
            return false
        }
        
        return true
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        for n in 0..<customFields.count {
            if l8rJomFOsEnjqZ2BIEXdFGSzAZ9FqhNh(index: n) {
                let bioDoc = EVWtDBsZZch41sb7sLgCIb9vNtdkveCn(docType: customFields[n].type!.rawValue, document: textFields[n].text!)
                bioDocArray.append(bioDoc)
            } else {
                bioDocArray.removeAll()
                return
            }
        }
        
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.extraFields.removeFirst()
        CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.docBioArray.append(contentsOf: bioDocArray)
        
        if CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.extraFields.count > 0 {
            let frameworkBundle = Bundle(for: CustomFormViewController.self)
            let formVC = CustomFormViewController(nibName: "CustomFormViewController", bundle: frameworkBundle)
            CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.viewController?.navigationController?.pushViewController(formVC, animated: true)
        } else {
            CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.operations?.removeFirst()
            nF3h6eXbjMliVEBjyFJsbabguW9ISjQK.OKCX84wdgylYmT6Ads5HzEh3NehTko39()
        }
    }
    
    
    private func xFi5tcAeqws7E6qBPiqrY5kiWlaKUxY5() {
        // Scroll view, vertical
        let scrollView = UIScrollView()
        containerView.addSubview(scrollView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .clear
        scrollView.layer.cornerRadius = 15
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: containerView.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -120)
        ])
        // 2. Content is a stack view
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 0
        stackView.distribution = .fill
        scrollView.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Attaching the content's edges to the scroll view's edges
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -16),
            
            // Satisfying size constraints
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        // Add arranged subviews:
        let infoLabel = UILabel()
        stackView.addArrangedSubview(infoLabel)
        infoLabel.text = "@Análise de informações@*\n *#\nPreencha os campos para prosseguir#"
        infoLabel.numberOfLines = 0
        infoLabel.attributedText = uCkQmn8kv3HtDNVBsW2jmMYp1hyJpU0g(string: infoLabel.text!)
        NSLayoutConstraint.activate([
            infoLabel.heightAnchor.constraint(equalToConstant: 60),
            infoLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 20),
            infoLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20)
        ])
        
        
        customFields = CYWMB0SsFx34NTnhJeGE9C5cS9Jy2lT6.extraFields.first![GISOperation.CUSTOM_FIELDS]!
        
        var tag = 0
        for item in customFields {
            let formField = FormField.lIV9VtO1zvI4wtrCk9wczXJouZWDtGbo(title: item.title, placeholder: item.desc)
            formField.heightAnchor.constraint(equalToConstant: 115).isActive = true
            
            formField.formField.formatPattern = item.mask
            formField.formField.tag = tag
            formField.formField.delegate = self
            
            if item.keyboard == .TEXT {
                formField.formField.keyboardType = .default
            } else {
                formField.formField.keyboardType = .numberPad
                formField.formField.addDoneCancelToolbar()
            }
            
            formField.formField.textColor = .black
            textFields.append(formField.formField)
            stackView.addArrangedSubview(formField)
            
            tag = tag+1
        }
        
        if customFields.contains(where: {$0.required == true }) {
            continueBtn.isEnabled = false
        }
    }
}

extension CustomFormViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        // Try to find next responder
        let nextResponder = textField.superview?.superview?.viewWithTag(nextTag) as UIResponder?
        
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        for n in 0..<customFields.count {
            if customFields[n].required && textFields[n].text == "" {
                continueBtn.isEnabled = false
                return
            }
        }
        continueBtn.isEnabled = true
    }
}

extension CustomFormViewController {
    func uCkQmn8kv3HtDNVBsW2jmMYp1hyJpU0g(fontSize: CGFloat = 14, string: String) -> NSMutableAttributedString {
        
        let formatedMessage = NSMutableAttributedString(string: string)
        
        //  Format the part between @
        if let openArrobaRange = string.range(of: "@"),
            let closeArrobaRange = string.range(of: "@", range: openArrobaRange.upperBound..<string.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3123499751, green: 0.2975655198, blue: 0.5166683197, alpha: 1), NSAttributedString.Key.font: UIFont.VkVsgovoB2PUmFHenU7jC2UnSGyhAc3f(ofSize: 24)], range: NSRange(openArrobaRange.upperBound..<closeArrobaRange.lowerBound, in: string))
        } else {
            NSLog("Could not get string range between @")
        }
        
        // Format the part between *
        if let openAsteriskRange = string.range(of: "*"),
            let closeAsteriskRange = string.range(of: "*", range: openAsteriskRange.upperBound..<string.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.font: UIFont.VkVsgovoB2PUmFHenU7jC2UnSGyhAc3f(ofSize: 8)], range: NSRange(openAsteriskRange.upperBound..<closeAsteriskRange.lowerBound, in: string))
        } else {
            NSLog("Could not get string range between *")
        }
        
        // Format the part between #
        if let openHashtagRange = string.range(of: "#"),
            let closeHashtagRange = string.range(of: "#", range: openHashtagRange.upperBound..<string.endIndex) {
            formatedMessage.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4669571519, green: 0.5273934603, blue: 0.6115893126, alpha: 1), NSAttributedString.Key.font: UIFont.uvBnLVLPutL5chlgh8I3zy7KqaOBfkpS(ofSize: 14)], range: NSRange(openHashtagRange.upperBound..<closeHashtagRange.lowerBound, in: string))
        } else {
            NSLog("Could not get string range between *")
        }
        
        formatedMessage.mutableString.replaceOccurrences(of: "@", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        formatedMessage.mutableString.replaceOccurrences(of: "*", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        formatedMessage.mutableString.replaceOccurrences(of: "#", with: "", options: NSString.CompareOptions(rawValue: 0), range: NSRange(location: 0, length: formatedMessage.mutableString.length))
        
        return formatedMessage
    }
}
