//
//  GisOperation.swift
//  GISPod
//
//  Created by VALID Certificadora on 18/03/20.
//

import Foundation

public enum FieldType: String {
    case CPF
    case FIRST_NAME
    case LAST_NAME
    case RG
    case MOTHERS_NAME
    case FULL_NAME
    case BIRTHDATE
}

public struct GISField {
    public let title: String
    public let desc: String
    public let mask: String
    public let keyboard: KeyboardStyle
    public var required: Bool = false
    public var type: FieldType?
    
    private init (title: String, desc: String, mask: String, keyboard: KeyboardStyle, required: Bool = false, type: FieldType) {
        self.title = title
        self.desc = desc
        self.mask = mask
        self.keyboard = keyboard
        self.required = required
        self.type = type
    }
    
    public static func FIRST_NAME(required: Bool = false) -> GISField {
        return GISField(title: "Nome", desc: "Nome", mask: "", keyboard: .TEXT, required: required, type: .FIRST_NAME)
    }
    
    public static func LAST_NAME(required: Bool = false) -> GISField {
        return GISField(title: "Sobrenome", desc: "Sobrenome", mask: "", keyboard: .TEXT, required: required, type: .LAST_NAME)
    }
    
    public static func FULL_NAME(required: Bool = false) -> GISField {
        return GISField(title: "Nome Completo", desc:"Insira seu nome completo", mask: "", keyboard: .TEXT, required: required, type: .FULL_NAME)
    }
    
    public static func MOTHERS_NAME(required: Bool = false) -> GISField {
        return GISField(title: "Nome da mãe",desc: "Insira o nome de sua mãe", mask: "", keyboard: .TEXT, required: required, type: .MOTHERS_NAME)
    }
    
    public static func CPF(required: Bool = false) -> GISField {
        return GISField(title: "CPF", desc: "Insira o número do CPF", mask: "999.999.999-99", keyboard: .NUMERIC, required: required, type: .CPF)
    }
    
    public static func RG(required: Bool = false) -> GISField {
        return GISField(title: "RG", desc: "Insira o número do RG", mask: "99.999.999-99", keyboard: .NUMERIC, required: required, type: .RG)
    }
    
    public static func BIRTHDATE(required: Bool = false) -> GISField {
        return GISField(title: "Data de Nascimento", desc: "Insira sua data de nascimento", mask: "99/99/9999", keyboard: .NUMERIC, required: required, type: .BIRTHDATE)
    }
    
}

public enum KeyboardStyle {
    case NUMERIC, TEXT
}
