//
//  OAuthModel.swift
//  GISPod
//
//  Created by VALID Certificadora on 20/03/20.
//  Copyright © 2020 VALID Certificadora. All rights reserved.
//

import Foundation

struct cUox7VyGxg673mkriMYs1fzdrj7dVKqQ: Codable {
    var access_token: String
    var token_type: String
    var refresh_token: String
    var expires_in: Int
    var scope: String
    var subjectId: String
}
