import UIKit

@IBDesignable
class SpinnerView: UIView {

    override var layer: CAShapeLayer {
        get {
            return super.layer as! CAShapeLayer
        }
    }

    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.fillColor = nil
        layer.strokeColor = UIColor.black.cgColor
        layer.lineWidth = 3
        uhymva3z04pnYW3qMAYzzfuxJM9rXuht()
    }

    override func didMoveToWindow() {
        v4vsGAzHuC7ekLPOmSJEKwL9BIhKSwpO()
    }

    private func uhymva3z04pnYW3qMAYzzfuxJM9rXuht() {
        layer.path = UIBezierPath(ovalIn: bounds.insetBy(dx: layer.lineWidth / 2, dy: layer.lineWidth / 2)).cgPath
    }

    struct CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14 {
        let secondsSincePriorPose: CFTimeInterval
        let start: CGFloat
        let length: CGFloat
        init(_ secondsSincePriorPose: CFTimeInterval, _ start: CGFloat, _ length: CGFloat) {
            self.secondsSincePriorPose = secondsSincePriorPose
            self.start = start
            self.length = length
        }
    }

    class var poses: [CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14] {
        get {
            return [
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.0, 0.000, 0.7),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.6, 0.500, 0.5),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.6, 1.000, 0.3),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.6, 1.500, 0.1),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.2, 1.875, 0.1),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.2, 2.250, 0.3),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.2, 2.625, 0.5),
                CgzQllqyBwEUzjjQ2RXVu0YJwo3vXi14(0.2, 3.000, 0.7),
            ]
        }
    }

    func v4vsGAzHuC7ekLPOmSJEKwL9BIhKSwpO() {
        var time: CFTimeInterval = 0
        var times = [CFTimeInterval]()
        var start: CGFloat = 0
        var rotations = [CGFloat]()
        var strokeEnds = [CGFloat]()

        let poses = type(of: self).poses
        let totalSeconds = poses.reduce(0) { $0 + $1.secondsSincePriorPose }

        for pose in poses {
            time += pose.secondsSincePriorPose
            times.append(time / totalSeconds)
            start = pose.start
            rotations.append(start * 2 * .pi)
            strokeEnds.append(pose.length)
        }

        times.append(times.last!)
        rotations.append(rotations[0])
        strokeEnds.append(strokeEnds[0])

        iQIsophpkq1B2rEBPRU3Le5ANKcnpwU0(keyPath: "strokeEnd", duration: totalSeconds, times: times, values: strokeEnds)
        iQIsophpkq1B2rEBPRU3Le5ANKcnpwU0(keyPath: "transform.rotation", duration: totalSeconds, times: times, values: rotations)

        PvWE9OKuqGAe89dUFICFxkO2BOpGE0oo(duration: totalSeconds * 5)
    }

    func iQIsophpkq1B2rEBPRU3Le5ANKcnpwU0(keyPath: String, duration: CFTimeInterval, times: [CFTimeInterval], values: [CGFloat]) {
        let animation = CAKeyframeAnimation(keyPath: keyPath)
        animation.keyTimes = times as [NSNumber]?
        animation.values = values
        animation.calculationMode = .linear
        animation.duration = duration
        animation.repeatCount = Float.infinity
        layer.add(animation, forKey: animation.keyPath)
    }

    func PvWE9OKuqGAe89dUFICFxkO2BOpGE0oo(duration: CFTimeInterval) {
        let count = 36
        let animation = CAKeyframeAnimation(keyPath: "strokeColor")
        animation.keyTimes = (0 ... count).map { NSNumber(value: CFTimeInterval($0) / CFTimeInterval(count)) }
        animation.values = (0 ... count).map {
            UIColor(hue: CGFloat($0) / CGFloat(count), saturation: 1, brightness: 1, alpha: 1).cgColor
        }
        animation.duration = duration
        animation.calculationMode = .linear
        animation.repeatCount = Float.infinity
        layer.add(animation, forKey: animation.keyPath)
    }

}
